//import User model
const	User = require('../models/User')

module.exports.createUserController = (request,response)=>{

	User.findOne({username : request.body.username},(err,result)=>{

			console.log(err) 
			console.log(result)
			if(result !== null && result.username === request.body.username){
				return response.send("Duplicate Username Found")
			} else {

				let newUser = new User({

					username:request.body.username,
					password:request.body.password
				})

				newUser.save((saveErr,savedUser)=>{

					console.log(savedUser)

					if(saveErr){
						return console.error(saveErr);
					} else {
						return response.send("Successful Registration!");
					}
				})
			}

		})
}

module.exports.getAllUserController = (request,response) => {
	User.find({})
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.getSingleUserController = (request,response) => {

	//mongoose has a query called findById() which works like find({_id:"id"})
	console.log(request.params.id) // the id passed from your url.
	User.findById(request.params.id)
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.updateSingleUserController = (request,response) => {

	console.log(request.params.id)
	/*
		Model.findByIdAndUpdate will do 2 things, first, look for the single item by its id , then add the update
		model.findByIdAndUpdate(id,{updates},{new:true})
		
		By default, without the third argument, findByIdAndUpdate returns the document before the updates were added.
		{new:true} -allows to return the updated document
	*/
	//updates object will contain the field and the value we want to update
	let updates = {

		username : request.body.username

	}

	User.findByIdAndUpdate(request.params.id,updates,{new:true})
	.then(updatedUser => response.send(updatedUser))
	.catch(err => response.send(err))
}