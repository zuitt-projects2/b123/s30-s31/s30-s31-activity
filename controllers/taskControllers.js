//import Task model
const Task = require('../models/Task')


module.exports.createTaskController = (request,response) => {


	console.log(request.body.name);

	//create a new Task document
		//check if there is a document with duplicate name field
		//Model.findOne() is a mongoose method similar to findOne in MongoDB
		//however, with model.findOne(), we can process the result via our API
		//.then() is able to capture the result of our query.
			//.then() is able to process the result and when that is returned, we can actually add another then() to process the next result.
		//.catch() is able to capture the error of our query.
		/*with then and catch no more else if for the error like this newUser.save((saveErr,savedUser)=>{

					console.log(savedUser)

					if(saveErr){
						return console.error(saveErr);
					} else {
						return response.send("Successful Registration!");
					}
				})*/

		Task.findOne({name : request.body})
		.then(result => {console.log(result) // returns null if no documents were found with the given criteria
			if(result !== null && result.name === request.body.name){
				return response.send("Duplicate Task Found")
			} else {

				//Created a new task object out of our Task model.
				//newTask has added methods for use in our application.
				let newTask = new Task({

					name:request.body.name
				})

				//.save() is a method from an object created by a model.
				//This will then allow us to save our document int our collection.
				//.save() can have an anonymous function and this can take 2 parameters
				//The first item: saveErr receives an error object if there was an creating our documents
				//the secon item is our saved documents.
				newTask.save()
				.then(result => response.send(result))
				.catch(err => response.send(err))
			}
		})
		.catch(err => response.send(err))

	
}


module.exports.getAllTaskController = (request,response) => {
	

	//Model.find() is a Mongoose method similar to MongoDB's find(). Is is able to retrieve all documents that will match the criteria
	Task.find({})
	.then(result => response.send(result))
	.catch(err => response.send(err))


	
}

module.exports.getSingleTaskController = (request,response) => {

	//mongoose has a query called findById() which works like find({_id:"id"})
	//mongoose queries such as model.find(), model.findOne(), model.findById() has a second argument for projections. and how it works is the same as in mongoDB
	console.log(request.params.id) // the id passed from your url.
	Task.findById(request.params.id,{_id:0,name:1})
	.then(result => response.send(result))
	.catch(err => response.send(err))
}

module.exports.updateStatusCompleteTaskController = (request,response) => {
	console.log(request.params.id)

	let updates = {

		status : 'Complete'

	}

	Task.findByIdAndUpdate(request.params.id,updates,{new:true})
	.then(updatedStatus => response.send(updatedStatus))
	.catch(err => response.send(err))
}
module.exports.updateStatusCancelTaskController = (request,response) => {
	console.log(request.params.id)

	let updates = {

		status : 'Cancel'

	}

	Task.findByIdAndUpdate(request.params.id,updates,{new:true})
	.then(updatedStatus => response.send(updatedStatus))
	.catch(err => response.send(err))
}