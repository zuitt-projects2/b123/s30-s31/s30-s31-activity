const mongoose = require('mongoose')

//Mongoose Schema
	/*
		Schemas, in mongoose context, determine the structure of the documents. It acts as a blueprint to how data/entries in our collection shoud look like. Gone are the days when worry if entry has "stock" or "stocks" as field names. Mongoose itself will notify us and disallow us to create documents that do not match the schema
	*/
	// Schema() is a a constructor from mongoose that will allow us to create a new Schema object.
	const taskSchema = new mongoose.Schema({
		/*
			Define the fields for the task document.
			This task document has a name and status field.
			name with the value to be a String
			status has curly brace because it has defined fields to follow:
					value's type:String,
					and , if not provided a value once the document was created it will default to value :"pending"
		*/

		name:String,
		status:{
			type:String,
			default:"pending"
		}
	});


	//Mongoose Model
		/*
			Models are used to connect your api to the corresponding collection in your database. It is a representation of the Task documents to be created into a new tasks collection.

			Models uses schemas to create/instantiate objects that correspond to the schema. By default, when creating the collection from your model, the collection name will be pluralized (ENGLISH ONLY)

			mongoose.model(<nameOfCollectionInAtlas>,<schemaToFollow>)

			Your models have methods that will help in quering, creatine or even editing your documents. Models are also
	
		*/

module.exports = mongoose.model("Task",taskSchema);

//module.exports will allow us to export file into anoter js file within our application.
//export the model to the other files.