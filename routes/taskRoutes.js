const express = require('express')

//Router() from express, allows us tje access to HTTP method routes.
//Router() will action as a middleware and our routing system.
const router = express.Router();

//Routes should only be concerned with our endpoints and our methods.
//The action to be done once a route is accessed should be in separate file, it should be in our controllers.
const taskControllers = require('../controllers/taskControllers')

//deconstruct
const {

	createTaskController,
	getAllTaskController,
	getSingleTaskController,
	updateStatusCompleteTaskController,
	updateStatusCancelTaskController
	
} = taskControllers
console.log(taskControllers)
router.post('/',createTaskController)
router.get('/',getAllTaskController)
router.get('/:id',getSingleTaskController)
router.put('/complete/:id',updateStatusCompleteTaskController)
router.put('/cancel/:id',updateStatusCancelTaskController)

//router holds all of our routes and can be exported and imported into another file.
module.exports = router