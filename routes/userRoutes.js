//import express and Router()
const express = require('express')
const router = express.Router(); 

const userControllers = require('../controllers/userControllers')

//deconstruct
const{createUserController,getAllUserController,getSingleUserController,updateSingleUserController} = userControllers
router.post('/', createUserController)
router.get('/', getAllUserController)

router.get('/:id',getSingleUserController)
router.put('/:id',updateSingleUserController)

module.exports = router