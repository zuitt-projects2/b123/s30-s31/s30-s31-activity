const express = require("express")
const mongoose = require("mongoose")

const app = express()

const port = 4000

//paste your mongodb connection string to connect() method of mongoose module.
//change <password> to your db password
//change myFirstDatabase to todoList123
//MongoDB upon connection will create the todoList123 db once we created document for it

mongoose.connect("mongodb+srv://ianartis29:ianartis@cluster0.mngou.mongodb.net/todoList123?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology:true
	}
)
//notification if connection to db is success or failed:
let db = mongoose.connection

//console.error.bind(console,<message>) - print error in both terminal and browser
db.on("error",console.error.bind(console,"connection error."))

//Output a message in the terminal if the connection is successful
db.once("open",()=>console.log("Connected to MongoDB"))

//Middleware - middleware, in expressjs context, are methods, functions that acts and adds feature to your application.
//handle json data from our client 
app.use(express.json())

const taskRoutes = require('./routes/taskRoutes')
console.log(taskRoutes)
//A middle ware to group all of our routes starting their endpoints with /tasks
app.use('/tasks',taskRoutes)

const userRoutes = require('./routes/userRoutes')
app.use('/users',userRoutes)

app.listen(port, ()=>console.log(`Server is running at port ${port}`));